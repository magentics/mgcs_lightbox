Magentics Lightbox for Prototype
================================

This script provides lightbox functionality based on [Prototype Framework](https://github.com/sstephenson/prototype).

The development was inspired by the fact of absence lightbox-like script which uses Prototype + Scriptaculous libraries (Magento 1.x combination) and will work fine with all versions of major browsers (especially IEs).

Bug reports, feature requests or contributions are highly appreciated.

Requirements
------------
- prototype.js 1.6.x.x or higher
- scriptaculous.js 1.7.1 or higher
- effects.js 1.7.1 or higher

Installation
------------
Use something like this in your theme's local.xml:

    <catalog_product_view>
        <reference name="head">
            <action method="addJs"><file>mgcs/lightbox/lightbox.js</file></action>
            <action method="addItem"><type>js_css</type><file>mgcs/lightbox/lightbox.css</file></action>
        </reference>
    </catalog_product_view>

Then change the thumbnails in your `media.phtml` to look like this:

    <a href="<?php echo $_image->getUrl(); ?>" rel="lightbox[product]" title="<?php echo $this->escapeHtml($_image->getLabel()) ?>">
        <img src="<?php echo $this->helper('catalog/image')->init($this->getProduct(), 'thumbnail', $_image->getFile())->resize(56); ?>" width="56" height="56" title="<?php echo $this->escapeHtml($_image->getLabel()) ?>" alt="<?php echo $this->escapeHtml($_image->getLabel()) ?>" />
    </a>

Especially look at the `rel="lightbox[product]"`, which causes the Lightbox to act on the link.

Credits
-------
Based on Tim Bezhashvyly's Lightbox
https://github.com/tim-bezhashvyly/lightbox-prototype

Which is based on Lightbox Slideshow v1.2
by Justin Barkhuff - http://www.justinbarkhuff.com/lab/lightbox_slideshow/
Orphaned on: 2008-01-11

Which is in its turn based on Lightbox v2.02
by Lokesh Dhakar - http://huddletogether.com/projects/lightbox2/
from: 2006-03-31